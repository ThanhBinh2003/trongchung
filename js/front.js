
(function ($, app) {
    var homeCls = function () {
        this.run = function () {
            this.init();
            this.bindEvents();
        };
        
        this.init = function () {
            
        };
        
        this.bindEvents = function () {
            initslider();
            toggle_play_pause();
            toggle_volume();
            toggle_dropdown_content();
            toggle_dropdown();
        };
        
        var toggle_play_pause = function () {
            var video = document.getElementById("fifth_video");
            $("#overlay").bind("click", function () {
                console.log(111);
                if (video.paused){
                    video.play();
                    document.getElementById("play_icon").style.display="none";
                    document.getElementById("overlay").style.opacity="0";
                }else {
                    video.pause();
                    document.getElementById("play_icon").style.display="block";
                    document.getElementById("overlay").style.opacity="0.5";
                }
            });
            $("#play_icon").bind("click", function () {
                console.log(111);
                if (video.paused){
                    video.play();
                    document.getElementById("play_icon").style.display="none";
                    document.getElementById("overlay").style.opacity="0";
                }else {
                    video.pause();
                    document.getElementById("play_icon").style.display="block";
                    document.getElementById("overlay").style.opacity="0.5";
                }
            });
        };
        var toggle_volume=function() {
            var video = document.getElementById("fifth_video");
            
            $("#mute").bind("click", function () {
                console.log(111);
                if (video.muted){
                    video.muted=!video.muted;
                    document.getElementById("mute").innerHTML='<i class="fas fa-volume-up"></i>';
                } else{
                    video.muted=!video.muted;
                    document.getElementById("mute").innerHTML='<i class="fas fa-volume-mute"></i>';
            
                }
            });
        };
        var toggle_dropdown= function() {
            var menu_show = false;
            $("#dropdown__menu__button").bind("click", function () {
                console.log("a");
                if (menu_show==false){
                    document.getElementById("dropdown__menu__content").style.display="block";
                    document.getElementById("dropdown__menu__button").innerHTML='<i class="fas fa-times"></i>';
                    menu_show=true;
                } else {
                    document.getElementById("dropdown__menu__content").style.display="none";
                    document.getElementById("dropdown__menu__button").innerHTML='<i class="fas fa-bars"></i>';
                    menu_show=false;
                }
            });
        }
        var toggle_dropdown_content= function(){
        
            $(".hasDropDown").bind("click", '*',function (e) {
                $(this).find('.dropdown__content').toggleClass('open');
                e.stopPropagation();
            });
            
        };
        var initslider = function(){
            var weChoice = new Swiper('.weChoice', {
                slidesPerView: 4,
                spaceBetween: 40,
                loop: true,
                loopFillGroupWithBlank: true,
                navigation: {
                    nextEl: '.story__next',
                    prevEl: '.story__prev',
                },
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                },
                breakpoints:{
                    320:{
                        slidesPerView: 2,
                        spaceBetween:10,
                    },
                    800:{
                        slidesPerView: 3,
                        spaceBetween:10,
                    },
                    1200:{
                        slidesPerView: 4,
                    },
                }
            });
            var news_slide = new Swiper('.news_slide', {
                slidesPerView: 'auto',
                spaceBetween: 40,
                centeredSlides: true,
                loop: false,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.news__next',
                    prevEl: '.news__prev',
                },
                breakpoints:{
                    320:{
                        slidesPerView: 1,
                        spaceBetween:5,
                        
                    },
                    800:{
                        slidesPerView: 2,
                        spaceBetween:10,
                    },
                    1200:{
                        slidesPerView: 3,
                    },
                }
            });
            
        }
    
    };
    $(document).ready(function () {
        var homeObj = new homeCls();
        homeObj.run();
        
        
    });
}(jQuery, $.app));
const gambitGalleryIsInView = el => {
    const scroll = window.scrollY || window.pageYOffset
    const boundsTop = el.getBoundingClientRect().top + scroll
    
    const viewport = {
        top: scroll,
        bottom: scroll + window.innerHeight,
    }
    
    
    const bounds = {
        top: boundsTop,
        bottom: boundsTop + el.clientHeight,
    }
    
    return ( bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom ) 
        || ( bounds.top <= viewport.bottom && bounds.top >= viewport.top );
}
document.addEventListener( 'DOMContentLoaded', () => {
    const video_container = document.querySelector( '.fifth__video' )
    const video = document.querySelector( '.video' )
    
    const handler = () => raf( () => {
        if (gambitGalleryIsInView( video_container )==false) {
            video.pause();
            document.getElementById("play_icon").style.display="block";
            document.getElementById("overlay").style.opacity="0.5";
        } else{
            video.play();
            document.getElementById("play_icon").style.display="none";
            document.getElementById("overlay").style.opacity="0";
        }
    } )
    
    handler()
    window.addEventListener( 'scroll', handler )
} )
const raf = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function( callback ) {
        window.setTimeout( callback, 1000 / 60 )
    }